import React from 'react';
import { useEffect, useState } from 'react';

import { Container } from './style';

import Dashboard from './components/dashboard/dashboard'
import Sidebar from './components/sidebar/sidebar'
import Content from './components/content/content'
import Signin from './components/sign_in/sign_in'

const EMAIL = 'qwe#qwe.com'
const PASSWORD = '123456'

const d = new Date()

const App = () => {

  const [authenticated, setAuthenticated] = useState(false)
  const [sidebarActive, setSidebarActive] = useState(true)

  useEffect(() => {
    let cookie: String = document.cookie

    if(cookie.length !== 0) {
      let userdata = cookie.split('=')
      
      if(userdata[0] === EMAIL && userdata[1] === PASSWORD) {
        setAuthenticated(true)
      }
    }

  }, [setAuthenticated])

  const signinHandler = (email: string, password: string) => {

    if(email === EMAIL && password === PASSWORD) {
      setAuthenticated(true)

      d.setTime(d.getTime() + 1440*60*1000)
      document.cookie = `${email}=${password}; expires=${d.toUTCString()};`
      console.log(d.toUTCString())
      return true
    }
    
    return false
  }

  const logoutHandler = () => {
    document.cookie = `${EMAIL}=${PASSWORD}; expires=Thu, 01-Jan-1970 09:00:01 GMT;`
    setAuthenticated(false)
  }

  const sidebarToggelHandler = () => {
    setSidebarActive(!sidebarActive)
  }

  return (
    <div>
      {
        !authenticated ? <Signin  callback={signinHandler}/> :
        <>
          <Dashboard 
          signinCallback={sidebarToggelHandler}
          logoutCallback={logoutHandler}/>
          <Container>
            <Sidebar sidebarState={sidebarActive}/>
            <Content />
          </Container>
        </>
      }

    </div>
  )
}

export default App;
