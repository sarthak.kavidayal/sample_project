import styled , { createGlobalStyle } from "styled-components";


export default createGlobalStyle`
    body {
        height: 100%;
        min-height: 100%;
    }
`

export const Container = styled.div`
    display: flex;
    width: 100%;
`