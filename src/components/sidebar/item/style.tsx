import styled from "styled-components";

export const Item = styled.div`
    height: 26px;
    border-radius: 5px;
    color: white;
    background-color: #353839;
    padding: 4px;
    margin: 4px 10px;
    width: 172px;
    display: flex;
    align-items: center;

    &:hover {
        cursor: pointer;
    }
`