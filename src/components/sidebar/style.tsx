import styled from "styled-components";

type Props = {
    sidebarState: boolean
}

export const Sidebar = styled.div<Props>`
    background-color: #414A4C;
    border-right: 1px solid black;
    width: 200px;
    height: 92vh;
    margin-left: ${props => props.sidebarState ? '0px' : '-200px'};
    transition: 300ms;
`