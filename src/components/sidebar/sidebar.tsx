import React from "react";

import { Sidebar } from "./style";

import Item from './item/item'

type Props = {
    sidebarState: boolean
}

const sidebar: React.FC<Props> = ({sidebarState}) => {
    return (
        <Sidebar sidebarState={sidebarState}>
            <Item />
            <Item />
            <Item />
            <Item />
            <Item />
            <Item />
        </Sidebar>
    )
}

export default sidebar