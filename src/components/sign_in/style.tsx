import styled from "styled-components";

export const Container = styled.div`
    widht: 100%;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const Signin = styled.div`
    border: 1px solid black;
    padding: 10px;

    & input {
        border: none;
        border-radius: 5px;
        width: 90%;
        background-color: #d1d1d1;
        padding: 10px;
    }

    & button {
        margin-top: 20px;
        margin-left: 38%;
        border: none;
        background-color: #141414;
        color: white;
        padding: 5px;
        border-radius: 5px;
    }

    & button:hover {
        cursor: pointer;
        background-color: #5e5e5e;
    }

    & button:disabled {
        background-color: #cccccc;
        color: #666666; 
    }

    & .hidden {
        display: none;
    }

    & .visible {
        color: red;
    }
`