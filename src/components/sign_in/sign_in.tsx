import React, { useState, useEffect } from "react";

import { Signin, Container } from "./style";

type Props = {
    callback: (email: string, password: string) => boolean
}

const SignIn: React.FC<Props> = ({ callback }) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [disable, setDisable] = useState(true)
    const [valid, setValid] = useState(true)

    useEffect(() => {
        if(email !== '' && password !== '') {
            setDisable(false)
        }
        else {
            setDisable(true)
        }
    }, [email, password])

    const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value)
    }

    const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value)
    }

    const submitHandler = () => {
        setValid(callback(email, password))
    }

    return(
        <Container>
            <Signin>
                <h3>Sign In</h3>
                <p>Username</p>
                <input placeholder="Username" onChange={emailHandler}/>
                <p>Password</p>
                <input placeholder="Password" type="password" onChange={passwordHandler}/>
                <p className={valid ? 'hidden' : 'visible'}>Username or password did not match.</p>
                <button disabled={disable} onClick={submitHandler}>Submit</button>
            </Signin>
        </Container>
    )
}

export default SignIn