import React, { useEffect, useState } from "react";

import Row from './row/row'

type Props = {
    callback: (
        fullName: any,
        phoneNumber: any,
        email: any,
        employmentStatus: any,
        salary: any
    ) => React.MouseEventHandler
    toggleRender: boolean
}

const Table: React.FC<Props> = ({callback, toggleRender}) => {

    const [localData, setLocalData] = useState<any[]>([])

    useEffect(() => {
        if(localStorage.getItem('userData') !== null) {
            // @ts-ignore: localStorage value error
            setLocalData(JSON.parse(localStorage.getItem('userData')))
        }
    }, [setLocalData, toggleRender])

    let counter = 0

    const renderedTable = (
        <div>
            {
                // @ts-ignore: localStorage value error
                localData.map(data => {
                    counter += 1

                    return(
                        <Row  
                        fullName={data.fullName}
                        phoneNumber={data.phoneNumber}
                        email={data.email}
                        employmentStatus={data.employmentStatus}
                        salary={data.salary ? data.salary : '-'}
                        heading={false}
                        callback={callback}
                        key={counter}
                        />
                    )
                })
            }
        </div>
    )

    return (
        <div>
            <Row 
            heading={true}
            callback={callback} />
            {renderedTable}
        </div>
    )
}

export default Table