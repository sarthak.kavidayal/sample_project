import React from "react";

import { RowStyle } from "./style";

type Props = {
    fullName?: string,
    phoneNumber?: number | string,
    email?: string,
    employmentStatus?: string,
    salary?: any
    callback: (
        fullName: any,
        phoneNumber: any,
        email: any,
        employmentStatus: any,
        salary: any
    ) => React.MouseEventHandler
    heading: boolean
}
 // @ts-ignore
const Row: React.FC<Props> = (props) => {

    const onClickHandler = () => {
        props.callback(props.fullName, props.phoneNumber, props.email, props.employmentStatus, props.salary)
    }

    return (
        props.heading ? 
        <RowStyle>
            <div className="name"><b>Full Name</b></div>
            <div className="phone"><b>Phone</b></div>
            <div className="email"><b>Email</b></div>
            <div className="status "><b>Employement Status</b></div>
            <div className="salary"><b>Salary</b></div>
            <div className="edit"><b>Action</b></div>
        </RowStyle> :
        <RowStyle>
            <div className="name">{props.fullName}</div>
            <div className="phone">{props.phoneNumber}</div>
            <div className="email">{props.email}</div>
            <div className="status ">{props.employmentStatus}</div>
            <div className="salary">{props.salary}</div>
            <div className="edit"><button onClick={onClickHandler}>
            Edit</button></div>
        </RowStyle>
    )
}

export default Row