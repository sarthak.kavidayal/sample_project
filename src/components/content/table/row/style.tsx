import styled from "styled-components";

export const RowStyle = styled.div`
    display: flex;
    width: 850px;

    & div {
        flex-grow: 1;
        border: 1px solid black;
        padding: 5px;
    }

    & .name {
        min-width: 160px;
    }

    & .phone {
        min-width: 120px;
    }

    & .email {
        min-width: 250px;
    }

    & .status {
        min-width: 170px;
    }

    & .salary {
        min-width: 100px;
    }

    & .edit {
        min-width: 50px;
    }

    & b {
        margin: 0px;
        padding: 0px;
    }
`