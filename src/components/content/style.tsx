import styled from "styled-components";

export const ContentStyle = styled.div`
    height: 92vh;
    background-color: #FAF9F6;
    flex-grow: 1;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    overflow-y: auto;
    overflow-x: auto;
`