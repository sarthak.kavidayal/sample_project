import React, {useState} from "react";

import { ContentStyle } from "./style";

import Form from './form/form'
import Table from "./table/table";

const Content = () => {
    const [editActive, setEditActive] = useState(false)
    const [editData, setEditData] = useState({})
    const [toggelRender, setToggelRender] = useState(true)

    // @ts-ignore: 
    const editHandler = (fullname, phoneNumber, email, employmentStatus, salary): React.MouseEventHandler<HTMLElement> => {
        setEditActive(true)
        setEditData({
            fullName: fullname,
            phoneNumber: phoneNumber,
            email: email,
            employmentStatus: employmentStatus,
            salary: salary === '-' ? '' : salary
        })
    }

    const toggleEditHandler = () => {
        setEditActive(false)
    }

    const toggelRenderHandler = () => {
        setToggelRender(!toggelRender)
    }

    return (
        <ContentStyle>
            <br />
            <h2>Details Form</h2>
            <Form 
            editActive={editActive} 
            editData={editData} 
            callback={toggleEditHandler}
            toggelRenderHandler={toggelRenderHandler}
            />
            <h2>Details Table</h2>
            <Table 
            callback={editHandler}
            toggleRender={toggelRender}/>
            <br/>
        </ContentStyle>
    )
}

export default Content