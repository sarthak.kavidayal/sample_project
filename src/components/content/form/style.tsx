import styled from "styled-components";

export const FormStyle = styled.form`
    border: 1px solid black;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    padding: 10px;

    & small {
        color: red;
    }

    & button {
        margin: 15px 0px 5px 0px;
        padding: 5px 0px 5px 0px;
    }
`