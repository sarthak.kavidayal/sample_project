import React, { useState, useEffect } from "react";
import { useForm, SubmitHandler } from "react-hook-form";

import { FormStyle } from "./style";

enum EmploymentStatus {
    student = "Student",
    employed = "Employed"
  }

export type FormInput = {
    fullName?: String
    phoneNumber?: number
    email?: String
    employmentStatus?: EmploymentStatus
    salary?: number 
}

type Props = {
    editActive: boolean
    editData: any
    callback: ()=>void
    toggelRenderHandler: ()=>void
}

const Form: React.FC<Props> = ({editActive, editData, callback, toggelRenderHandler}) => {

    const [salaryFieldActive, setSalaryFieldActive] = useState(false)
    const { register, formState: { errors }, handleSubmit, reset } = useForm();

    useEffect(() => {
        if(editActive) {
            if(editData.employmentStatus === 'employed'){
                setSalaryFieldActive(true)
            }

            reset(editData)
        }
    }, [editActive, editData])

    const onSubmit: SubmitHandler<FormInput> = ({fullName, phoneNumber, email, employmentStatus, salary}) => {

        const data = {
            fullName: fullName,
            phoneNumber: phoneNumber,
            email: email,
            employmentStatus: employmentStatus,
            salary: salary
        }

        if(localStorage.getItem('userData') === null) {
            localStorage.setItem('userData', JSON.stringify([]))
        }

        let localData = []
        // @ts-ignore: localStorage value error
        localData = JSON.parse(localStorage.getItem('userData'))
        localData.push(data)
        localStorage.setItem('userData', JSON.stringify(localData))

        reset()
        toggelRenderHandler()
    }

    const onEdit: SubmitHandler<FormInput> = ({fullName, phoneNumber, email, employmentStatus, salary}) => {
        // @ts-ignore: localStorage value error
        let localData = JSON.parse(localStorage.getItem('userData'))

        // @ts-ignore: llocal data
        localData.forEach(data => {

            if (data.fullName === editData.fullName && data.phoneNumber === editData.phoneNumber && data.email === editData.email) {
                data.fullName = fullName
                data.phoneNumber = phoneNumber
                data.email = email
                data.employmentStatus = employmentStatus
                data.salary = salary
            }
        })

        localStorage.setItem('userData', JSON.stringify(localData))
        callback()
        reset({
            fullName: '',
            phoneNumber: '',
            email: '',
            employmentStatus: '',
            salary: ''
        })
        toggelRenderHandler()
    }

    const employmentStatusChangeHandler = (event: React.ChangeEvent<HTMLSelectElement>) => {
        if(event.target.value === 'student') {
            setSalaryFieldActive(false)
            reset({salary: ''})
        }
        else {
            setSalaryFieldActive(true)
        }
    }

    return(
        <FormStyle onSubmit={handleSubmit(onSubmit)} >
            <label>Full Name</label>
            <input {...register("fullName", {required: "This field is required!"})} />
            <small>{errors.fullName ? errors.fullName.message : undefined}</small>
            <label>Phone Number</label>
            <input {...register("phoneNumber", {
                required: 'This field is required',
                pattern: {
                  value: /^\d{10}$/,
                  message: "Enter valid 10 digit phone number."
                }
            })} />
            <small>{errors.phoneNumber ? errors.phoneNumber.message : undefined}</small>
            <label>Email</label>
            <input {...register("email", {
                required: 'This field is required.',
                pattern: {
                  value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                  message: "Enter valid email."
                }
            })} />
            <small>{errors.email ? errors.email.message : undefined}</small>
            <label>Employment Status</label>
            <select {...register("employmentStatus", {required: true})} onChange={employmentStatusChangeHandler}>
                <option value="student">Student</option>
                <option value="employed">Employed</option>
            </select>
            <label >Salary</label>
            <input {...register("salary", {
                required: salaryFieldActive ? 'This field is required.' : false,
                pattern: {
                  value: /\d+/,
                  message: "Salary should be numerical."
                }
            })} 
            disabled={!salaryFieldActive} />
            <small>{errors.salary ? errors.salary.message : undefined}</small>
            {editActive ? <button onClick={handleSubmit(onEdit)}>Save Edit</button> : <button type="submit">Add</button>}
        </FormStyle>
    )
}

export default Form