import styled from 'styled-components';

export const Dashboard = styled.div`
    width: 100%;
    background-color: black;
    color: white;
    margin: 0px;
    height: 8vh;
    display: flex;
    justify-content: space-between;

    & h2 {
        padding: 0px;
        margin: 0px;
    }

    & button {
        padding: 5px;
        margin: 0px 15px;
    }

    & .title {
        display: flex;
        align-items: center;
    }

    & .log_out {
        margin-right: 15px;
    }

    & .log_out:hover {
        cursor: pointer;
    }
`