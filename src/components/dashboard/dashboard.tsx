import React from "react";

import { Dashboard } from './style'

type Props = {
    signinCallback: ()=> void
    logoutCallback: ()=> void
}

const dashboard: React.FC<Props> = ({signinCallback, logoutCallback}) => {
    return (
        <Dashboard>
            <div className="title">
                <button onClick={signinCallback}>--</button>
                <h2>Dashboard</h2>
            </div>
            <div className="log_out" onClick={logoutCallback}>
                <h3>Log Out</h3>
            </div>
        </Dashboard>
    )
}

export default dashboard