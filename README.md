## Sample project

A react project using typescript.

# Login credential

```
    email = qwe#qwe.com
    password = 123456
```


# Tasks completed

1. Cookie created for 24 hours login.
2. Logout functionality created.
3. collapsable sidebar
4. Form created using react-hook-form library
5. Data from form saved in local storege.
6. Created table to display form data and edit it.
7. Applied validations in form.
